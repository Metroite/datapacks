## growontoptree

If players are inside wood, they get teleported up recursively! This can be misused. Not recommended.

## Version

Minecraft 1.13

## Performance

Tier 1/5

## Authors

**Metroite**
