## universaldyeing

I didn't create this. I modified it to make it more lightweight.
This is a QoL datapack. You can redye your dyeable stuff.

## Version

Minecraft 1.13

## Authors

**MSpaceDev**, modified by **Metroite**
```
https://xisumavoid.com/vanillatweaks/
```