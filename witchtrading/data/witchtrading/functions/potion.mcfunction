#Mysterious potion random effect
effect clear @s minecraft:wither
effect give @s[scores={rng=1}] minecraft:luck 1200 1 false
effect give @s[scores={rng=2}] minecraft:resistance 1200 0 false
effect give @s[scores={rng=3}] minecraft:unluck 1200 1 false
effect give @s[scores={rng=4}] minecraft:slow_falling 1200 1 false
effect give @s[scores={rng=5}] minecraft:glowing 1200 0 false
effect give @s[scores={rng=6}] minecraft:hunger 1200 0 false
effect give @s[scores={rng=7}] minecraft:blindness 1200 0 false
effect give @s[scores={rng=8}] minecraft:mining_fatigue 1200 1 false
effect give @s[scores={rng=9}] minecraft:haste 1200 1 false
effect give @s[scores={rng=10}] minecraft:dolphins_grace 1200 0 false
effect give @s[scores={rng=11}] minecraft:weakness 1200 2 false
effect give @s[scores={rng=12}] minecraft:jump_boost 1200 1 false
effect give @s[scores={rng=13}] minecraft:slowness 1200 0 false
effect give @s[scores={rng=14}] minecraft:night_vision 1200 0 false
effect give @s[scores={rng=15}] minecraft:health_boost 1200 0 false
