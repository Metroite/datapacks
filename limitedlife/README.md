## limitedlife

limitedlife limits your max health every time a player dies. Max health can be restored if a *Bat Wing* is thrown into a water filled cauldron under an End Crystal. The Absorption effect will be needed!

## Version

Minecraft 1.13

## Performance

Tier 4/5

## Authors

**Metroite**
