## doubleslabcrafting

I didn't create this. I modified it to make it more lightweight.
This pack allows you to craft double slabs.

*S - Any Slab*

**Any Double Slab:**
```
   
 SS
 SS
```

## Version

Minecraft 1.13

## Authors

**MSpaceDev**, modified by **Metroite**
```
https://xisumavoid.com/vanillatweaks/
```