## morsecode

A very smooth way of wireless communication. Put a redstone_torch in a item_frame on a gold_block (sender) with a button on top or a iron_block (receiver) with a redstone_wire on top. The ItemRotation will decide which one of eight frequencies to use.

## Version

Minecraft 1.13

## Performance

Tier 2/5

## Authors

**Metroite**
