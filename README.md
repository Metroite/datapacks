# Metroites Datapacks

These are my datapacks for Minecraft which I have coded:
```
earlyleatherarmor
graves
growontoptree
limitedlife
loud2x2tnt
morsecode
signalfire
witchtrading
```

These were just modified by me:
```
doubleslabcrafting - MSpaceDev
endermangrief - MSpaceDev
moremobheads - MSpaceDev
universaldyeing - MSpaceDev
```

These have nothing to do with me and I should actually remove them:
```
minecraft - Mojang
```

You could alway find more useful information inside every datapack.

## Getting Started

Every folder in here is its own datapack. Datapack specific information can be found inside these datapacks.

### Installing

Its easy.

Move the datapack you wish to use inside your "datapacks" folder of your wished save file found in .minecraft\saves.

```
C:\Users\<User>\AppData\Roaming\.minecraft\saves\<world name>\datapacks
```

Now just load your save file in your Minecraft client and execute the following command:

```
/datapack enable <name of the added datapack>
```

Have fun trying out the new stuff you've added!

## Version

These datapacks were created in 1.13, they might be working in newer versions of Minecraft.

## Authors

**Metroite**

## License

This project is licensed under the GNU GENERAL PUBLIC License - see the [LICENSE.md](LICENSE.md) file for details
