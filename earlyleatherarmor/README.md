## earlyleatherarmor

As the name suggests, this datapacks makes leather armor actually worth crafting.

*/ - Stick*

*L - Leather*

**Leather Helmet:**
```
/L/
/ /
   
```

**Leather Chestplate:**
```
/ /
/L/
///
```

**Leather Leggings:**
```
/L/
/ /
/ /
```

**Leather Boots:**
```
   
/ /
L/L
```

## Version

Minecraft 1.13

## Performance

Tier 1/5

## Authors

**Metroite**
